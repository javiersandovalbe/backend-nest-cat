import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength, MaxLength,IsEmail, IsNumber } from 'class-validator';

export class CreateUserDto {

    @ApiProperty()
    @IsEmail()
    email: string;
    
    @ApiProperty()
    @IsString()
    @MinLength(8)
    @MaxLength(45)
    password: string;


    @ApiProperty()
    @IsString()
    firstName: string;

    @ApiProperty()
    @IsString()
    lastName: string;

    @ApiProperty()
    @IsString()
    identification: string;

    @ApiProperty()
    @IsString()
    phone: string;
}
