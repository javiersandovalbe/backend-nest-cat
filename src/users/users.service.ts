import { Model } from 'mongoose';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { hash } from 'bcrypt';

@Injectable()
export class UsersService {

  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async create(createUserDto: CreateUserDto) {
    const userExist = await this.userModel.findOne({ email: createUserDto.email });
    if(userExist) throw new BadRequestException(`User with email: ${createUserDto.email} already exists`);

    let createdUser = new this.userModel(createUserDto);
    const hashPass = await hash(createdUser.password, 10);
    createdUser.password = hashPass;
    const {email, firstName, lastName, identification, phone} = await createdUser.save();
    return { email, firstName, lastName, identification, phone };
  }

  async findAll() {
    return await this.userModel.find();
  }

  async findOne(email: string) {
    const user = await this.userModel.findOne({email: email});
    if(!user) throw new NotFoundException('User not found!');

    return user;
  }
}
