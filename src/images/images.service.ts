import { HttpService } from '@nestjs/axios/dist';
import { Injectable, Logger } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, lastValueFrom } from 'rxjs';
import { environment } from '../environment';
import { Image } from './entities/image.entity';

@Injectable()
export class ImagesService {

  private readonly logger = new Logger(ImagesService.name);
  constructor(private readonly httpService: HttpService) {}

  async findOne(id: string) {
    const { data: response } = await lastValueFrom(
      this.httpService
        .get(`${environment.API_CAT}${environment.IMAGE_ENDPOINT}/search?limit=10&breed_ids=${id}&api_key=${environment.API_KEY}`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'Ocurrió un error';
          }),
        ),
    );
    return response;
  }
}
