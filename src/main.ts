import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import helmet from 'helmet';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
  .setTitle('Api - prueba técnica')
  .addBearerAuth()
  .setDescription('Documentacion de los endpoints prueba tecnica XpertGroup')
  .setVersion('1.0')
  .addTag('Cats api')
  .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.enableCors();
  app.use(helmet());

  await app.listen(3000);
}
bootstrap();
