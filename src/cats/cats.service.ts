import { HttpService } from '@nestjs/axios/dist';
import { Injectable, Logger } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, lastValueFrom } from 'rxjs';
import { environment } from '../environment';
import { Cat } from './entities/cat.entity';

@Injectable()
export class CatsService {

  private readonly logger = new Logger(CatsService.name);
  constructor(private readonly httpService: HttpService) {}

  async findAll(): Promise<Cat[]> {
    const { data: response } = await lastValueFrom(
      this.httpService
        .get(`${environment.API_CAT}${environment.BREEDS_ENDPOINT}`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'Ocurrió un error';
          }),
        ),
    );
    return response;
  }

  async findOne(id: string):Promise<Cat> {
    const { data: response } = await lastValueFrom(
      this.httpService
        .get(`${environment.API_CAT}${environment.BREEDS_ENDPOINT}/${id}`)
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'Ocurrió un error';
          }),
        ),
    );
    return response;
  }

}
