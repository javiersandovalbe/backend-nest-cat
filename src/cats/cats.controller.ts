import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { CatsService } from './cats.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards';

@ApiTags('Cats')
@Controller('/api/cat')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get('/breeds')
  findAll() {
    return this.catsService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get('/breeds/:id')
  findOne(@Param('id') id: string) {
    return this.catsService.findOne(id);
  }
}
