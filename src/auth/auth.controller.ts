import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards';
import { User as UserEntity } from '../users/entities/user.entity';
import { User } from '../common/decorators';

@ApiTags('Authorization')
@Controller('api/auth')
export class AuthController {

    constructor(
        private readonly authService: AuthService
    ){}

    @UseGuards(AuthGuard('local'))
    @Post('login')
    async login(@User() @Body() user: UserEntity){
        const data = await this.authService.login(user);
        const { accessToken } = data;

        return {
            message: 'Login successful',
            accessToken
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @Get()
    profile(@User() user: UserEntity){
        return {
            message: 'Your profile data',
            user
        }
    }
}
