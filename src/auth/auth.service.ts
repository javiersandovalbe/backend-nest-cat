import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/entities/user.entity';
import { compare } from 'bcrypt';

@Injectable()
export class AuthService {

    constructor(
        private readonly userService: UsersService, 
        private readonly jwtService: JwtService
    ){}

    async validateUser(email: string, password: string){
        const user = await this.userService.findOne(email);
        if(user && await compare(password, user.password)){
            return user;
        }

        return null;
    }

    login(user: User){
        const { email, ...rest } = user;
        const payload = { sub: email};

        return {
            accessToken: this.jwtService.sign(payload)
        }
    }
}
